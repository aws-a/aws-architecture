# AWS serverless设计-S3

## 能从存储桶名称中猜出用法吗？
这和命名变量是一样的，但是如果名字合适的话，如果你看桶名不知道它是做什么用的，你就得去检查它了。如果您决定在生产存储桶中包含生产，则可以减少操作数量。

![img](./img/api1.png) 

## 是否将存储桶放置在您想要的区域？
ALB 日志只能输出到同一区域的存储桶中。
https://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/application/load-balancer-access-logs.html
如果您使用VPC终端节点访问S3，从EC2的访问会更快但请注意，它只能在与VPC相同区域的存储桶中使用。
https://aws.amazon.com/jp/premiumsupport/knowledge-center/s3-maximum-transfer-speed-ec2/


## 存储等级合理吗？
不经常访问的对象可以通过Standard-Infrequent Access,One Zone-Infrequent Access等方式保存，以降低成本。
（注意One Zone-Infrequent Access会因为AZ故障而无法访问）
https://aws.amazon.com/jp/s3/storage-classes/?nc1=h_ls


## 版本控制设置是否合适？
如果您不小心删除了一个对象，如果启用了版本控制，您可以撤消它。但是，您还将为该对象的过去版本付费。
https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/Versioning.html


## 生命周期规则是否合适？
第一个月访问频繁，之后访问频率急剧下降。在这种情况下，将生命周期规则设置为低频的存储类将降低成本。如果您想在一定时间后删除日志等，您还可以设置删除规则。
https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/object-lifecycle-mgmt.html

## 加密设置是否合适？
加密您的重要数据。 不过好像有各种限制，所以要小心（我不是很熟悉，就这样吧……）
https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/bucket-encryption.html

## Block public access设置是否合适？
如果你打开这个，在存储桶策略/ACL中错误地设置为公共
当你允许公共访问时会给你一个错误。
https://dev.classmethod.jp/articles/s3-block-public-access/

![img](./img/s1.png) 

## 禁用ACL
我们禁用ACL，会很简单，因为你只能通过bucket策略来控制访问权限，而上传的时候，你就麻烦了

https://aws.amazon.com/jp/about-aws/whats-new/2021/11/amazon-s3-object-ownership-simplify-access-management-data-s3/
https://dev.classmethod.jp/articles/s3-bucket-owner-enforced/

## 最小化存储桶策略（Policy）赋予的权限
当您不知道自己需要什么权限时，设置这样的存储桶策略很容易，但这很危险。只允许最小的写入权限。