# AWS serverless设计-APIGateWay
**这个部分比较杂乱，我整理几个经常遇见的部分**

1. Stage名称只能包含字母数字、连字符和下划线。最大长度为 128 个字符。

2. /ping 和 /sping 路径仅用于服务健康检查。

3. API Gateway 目前将日志事件限制为 1024 字节。大于 1024 字节的日志事件（例如请求或响应的正文）会在发送到 CloudWatch Logs 之前被 API Gateway截断。

4. 目前，CloudWatch 指标将维度名称和值限制为 255 个有效 XML 字符（有关更多信息，请参阅 CloudWatch 用户指南）。维度值是用户定义的名称函数，包括API名称、标签（阶段）名称和资源名称。选择这些名称时，请注意不要超出 CloudWatch 指标限制。
https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/monitoring/cloudwatch_concepts.html#Dimension

5. 映射模板的最大大小为300KB。


6. 请求 URL 查询字符串中不支持纯文本管道字符 (|)，必须进行 URL 编码。请求 URL 查询字符串不支持分号字符 (;)，这会导致数据拆分。

7. 使用 API Gateway 控制台测试 API 时，后端会抛出自签名证书、不在证书链中的中间证书或其他无法识别的证书相关异常，可能会返回“未知端点错误”响应。

8. API Gateway 支持大多数 OpenAPI 2.0 和 OpenAPI 3.0 规范，但以下情况除外：
   + 路径段只能包含字母数字、连字符、句点、逗号、冒号和花括号。 
   path 参数必须是单独的路径段。
   例如，
   “/{path_parameter_name}”是有效的
   “{path_parameter_name}”无效

  + 型号名称只能包含字母数字。

  + 不支持> 默认关键字。

  + API Gateway 对您使用 Lambda 集成或 HTTP 集成的处理方式施加以下限制。

  + 标题名称和查询参数以区分大小写的方式处理。