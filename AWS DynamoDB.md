
# AWS serverless设计-DynamoDB

## 全体概念图
![img](./img/a1.png)


## 主键
**我觉得首先要注意的是DynamoDB 不适合“在两个或多个列（属性）中进行条件搜索的查询，其记录（项目）不能总是被那些规范唯一标识的主键”的要求频繁取出。
这样可以可以为每个表指定Hash Key或者外加一个Sort Key**
1. 仅哈希键

2. 哈希键和排序键

    >在其中一种模式中设置主键。Hash Key也称为Partition Key，可以理解为每个Partition Key分片。因此，建议将分区键设置为均匀分布查询的键。类似于传统数据库路的PK键。
     
    >Sort Key排序键也称为范围键。很多时候有hashkey我觉得足够的。如果表结构对有序要求很多，我建议额外设置这个键，要不然不设置，少设置最好。设置多了会增加RCU

![img](./img/d2.png)

**然后HashKey&SortKey必须是一个唯一键**
1. 既然是主键，自然要通过单独Hash Key或Hash Key & Sort Key（复合主键）来唯一标识记录。
2. 如果插入重复的唯一键，对应的记录将被覆盖（更新）。这个特点和大部分Nosql的性质是一样的。比如redis MongoDB
3. Scan随着记录数的增加而性能下降，Read Capacity Unit变大，费用增加，所以最好精准查询。不要动不动就scan，其实DynamoDb的花费是很高的。
4. [DynamoDB项目大小和格式](https://docs.aws.amazon.com/zh_cn/amazondynamodb/latest/developerguide/CapacityUnitCalculations.html)


## 全局索引
GSI是一个可以创建不同Hash Key和Sort Key的表的函数。可以在以后自由添加或删除它，而不仅仅是在创建表时。允许hashkey和sortkey重合，使用GSI读取会将RCU减半。使用GSI写入表会使WCU加倍。

除全局索引外还有本地二级索引 (LSI)
只能在建表时创建
